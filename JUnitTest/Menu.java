package JUnitTest;

public class Menu {
    private String foodName;
    private String description;
    public Menu(String foodName, String description) {
        this.foodName = foodName;
        this.description = description;
    }
    public String getFoodName() {
        return this.foodName;
    }
    public String getDescription() {
        return this.description;
    }
}