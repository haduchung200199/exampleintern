package JUnitTest;

import java.nio.charset.StandardCharsets;

/**
 * check if a String contains number
 */
public class ContainsNumberValidator {
    public Boolean doesContainNumber(String string) {
        byte[] asciiCheck = string.getBytes(StandardCharsets.US_ASCII);
        for(int i = 0;i<string.length();i++){
            if(asciiCheck[i]>=48 && asciiCheck[i]<=57){
                return true;
            }
        }
        return false;
    }
}
