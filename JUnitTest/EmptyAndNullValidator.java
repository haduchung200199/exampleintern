package JUnitTest;

public class EmptyAndNullValidator<T>{
    public Boolean isEmptyOrNull(T parameter){
       return parameter.toString().trim().isEmpty() || parameter.toString() == null;
    }
}
