package JUnitTest;

import junit.framework.TestCase;

import java.io.UnsupportedEncodingException;

public class TestMenu extends TestCase {
    public TestMenu(String name) {
        super(name);
    }
    /**
     * to confirm the name not null
     */
    public void testNotEmptyOrNull() { //+method testEmpty
        Menu menu = new Menu("BeefSteak", "Nice");
        EmptyAndNullValidator<String> emptyAndNullValidator = new EmptyAndNullValidator<>();
        assertFalse(emptyAndNullValidator.isEmptyOrNull(menu.getFoodName()));
        assertFalse(emptyAndNullValidator.isEmptyOrNull(menu.getDescription()));
    }

    /**
     * to test if the input value is empty or null
     */
    public void testEmptyAndNull(){
        Menu menu = new Menu("", null);
        EmptyAndNullValidator<String> emptyAndNullValidator = new EmptyAndNullValidator<String>();
        assertTrue(emptyAndNullValidator.isEmptyOrNull(menu.getFoodName()));
        assertTrue(emptyAndNullValidator.isEmptyOrNull(menu.getDescription()));
    }

    /**
     *check if string contains numbers
     */
    public void testIfContainsNumber() {//update example
        Menu menu = new Menu("Pho123", "Ni2c3e4");
        ContainsNumberValidator containsNumberValidator = new ContainsNumberValidator();
        //check if foodName is already true or not
        //this line will be wrong
        assertTrue(containsNumberValidator.doesContainNumber(menu.getFoodName()));
        assertTrue(containsNumberValidator.doesContainNumber(menu.getDescription()));
    }

    /**
     * test if String does not contain number
     */
    public void testIfNotContainsNumber(){
        Menu menu = new Menu("Pho", "Nice");
        ContainsNumberValidator containsNumberValidator = new ContainsNumberValidator();
        //check if foodName is already true or not
        //this line will be wrong
        assertFalse(containsNumberValidator.doesContainNumber(menu.getFoodName()));
        assertFalse(containsNumberValidator.doesContainNumber(menu.getDescription()));
    }
}
