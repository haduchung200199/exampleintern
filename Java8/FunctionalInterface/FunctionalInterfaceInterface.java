package Java8.FunctionalInterfacePackage;

import java.util.Objects;

/**
 * functional interface contains only one abstract class of itself
 * @param <T>
 */
@FunctionalInterface
interface FunctionalInterfaceInterface<T>{
    //the one and the only abstract class
    int validateEmpty(T object);

    //    //toString and equals are methods of Java.lang.Object, not the FunctionalInterfaceInterface itself
//    String toString();
//    boolean equals(Object object);
//    //static and default will not affect Functional interface's principle.
//    static void doSomething() {
//    }
//
//    default void doSomething2() {
//
//    }
}
