package Java8.FunctionalInterfacePackage;

import java.util.stream.IntStream;

/**
 * we will use functional interface to demo lamda expression
 */
public class FunctionalInterfaceDemo {
    public static void main(String[] args){
        //use lamda expresssion to create Functional interface's instance
        //"result"  parameter will be known as integer
         FunctionalInterfaceInterface functionalInterface = result -> {
             //overide the method
             return result.toString().trim().isEmpty() ? 1 : 0;
         };
         //call the method.
         if(functionalInterface.validateEmpty("")==1){
             System.out.println("null");
         }
         else System.out.println("not null");
    }

}

