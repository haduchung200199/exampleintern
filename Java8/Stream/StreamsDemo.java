package Java8.JavaStreamsPackage;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * demo for Java Stream
 */
public class StreamsDemo {
    public static void main(String[] args){
        //create a List
        List<Integer> list = new LinkedList();
        //add list
        list.add(5);
        list.add(10);
        list.add(13);
        list.add(2);
        list.add(4);
        list.add(6);
        list.add(3);
        list.add(9);
        list.add(7);
        list.add(8);
        list.add(17);
        list.add(1);
        //count the Prime number using lamda expression and stream to shorten the code
        long count = list.stream().filter(number -> (number % 2 != 0) && (number < 11) && (number > 2) ).count();
        System.out.println(count);

        //demo without using stream
        StreamsDemo streamsDemo = new StreamsDemo();
        streamsDemo.countPrimeNumbers(list);

    }

    /**
     * code without using stream
     * @param list
     */
    public void countPrimeNumbers(List<Integer> list) {
        long count = 0;
        for (int number : list) {
            if (number % 2 == 0 && number < 11 && number >2) {
                count++;
            }
        }
        System.out.println(count);
    }
}
