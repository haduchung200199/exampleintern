package HashCodeDemo;

import java.util.*;

public class HashCodeExample {
    public static void main(String[] args){
        //create a hashset
        Set<Menu> list = new HashSet<Menu>();
        //add element to hashset
        list.add(new Menu(123,"BeefSteak"));
        //the element with the same id will not be added.
        list.add(new Menu(123,"BanhXeo"));
        //different id with the same name will still be printed
        list.add(new Menu(456,"Pho"));

        for(Menu menu:list){
            System.out.println(menu);
        }
    }
}
