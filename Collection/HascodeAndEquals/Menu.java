package HashCodeDemo;

import java.util.Objects;

public class Menu {
    private long id;
    private String nameMenu;
    //constructor with variables
    public Menu(long id, String nameMenu) {
        this.id = id;
        this.nameMenu = nameMenu;
    }
    //constructor without variables
    public Menu() {
    }
    //getter and setter
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNameMenu() {
        return nameMenu;
    }

    public void setNameMenu(String nameMenu) {
        this.nameMenu = nameMenu;
    }
    //overide equal to know if the two elements are the same
    @Override
    public boolean equals(Object object) {
        //the instance is equal to itself (if this is object)
        if (this == object) return true;
        //check if the two instances are in the same class or not. if not, return false
        if (!(object instanceof Menu)) return false;
        //check if the parameter instance is null
        if(object == null) return false;
        //this mean two instances are in the same class, not null.
        Menu menu = (Menu) object;
        //only check if the id are the same.
        return id == menu.id;
    }
    //make text to string
    public String toString(){
        return "ID: " + id+" Menu name: "+nameMenu+" ";
    }

    //overide hasCode to prevent the same elements
    @Override
    public int hashCode() {
        int hash = 7;
        return hash = 31*hash + Objects.hashCode(this.id);

    }
}
