import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public class SortedSetDemo {
    public static void main(String[] args){
        //create an Arraylist to store numbers
        List<Integer> list = new ArrayList<>();
        list.add(9);
        list.add(4);
        list.add(5);
        list.add(3);
        list.add(6);
        list.add(2);
        list.add(7);
        list.add(8);
        list.add(0);
        //the same value will not be added twice -> print will only have one number "9"
        list.add(9);
        //create Treeset with list's element
        SortedSet sortedSet = new TreeSet(list);
        //the element of treeset will be arranged ASC automatically
        System.out.println(sortedSet);
        //return the first element then print
        System.out.println(sortedSet.first());
        //return the last element then print
        System.out.println(sortedSet.last());

    }
}
