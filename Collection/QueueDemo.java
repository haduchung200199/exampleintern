import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

public class QueueDemo {
    public static void main(String[] agrs){
        //Create a queue by upcasting linkedlist
        Queue<String> queue = new LinkedList<String>();
        //Queue will insert last and delete first
        //add the element to the last.
        queue.add("3");
        queue.add("4");
        queue.add("1");
        System.out.println(queue);
        //remove the first element of the list
        queue.remove();
        System.out.println(queue);
        //we can not use this line since Queue is based on FIFO
        //so the order can not be changed
        //Collections.sort(queue);
        //retrieves and removes the head of the queue (the deleted one)
        System.out.println(queue.poll());
        System.out.println(queue.poll());
        //retrieves the head of the queue. If null, returns "null"
        System.out.println(queue.peek());
        //retrieves the head of the queue. If null, throws NoSuchElementExceltion
        System.out.println(queue.element());
    }
}
