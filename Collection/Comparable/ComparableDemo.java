import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ComparableDemo {
    public static void main(String[] agrs){
        //create a new ArrayList
        List<Menu> list = new ArrayList<Menu>();
        //add elements to ArrayList
        list.add(new Menu("BeefSteak",200000));
        list.add(new Menu("Pho",50000));
        list.add(new Menu("Pasta",80000));
        list.add(new Menu("Salad",90000));
        //sort by foodName using CompareTo
        Collections.sort(list);
        for(Menu menu:list ){
            System.out.println(menu.toString());
        }
    }
}
