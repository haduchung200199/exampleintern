/**
*A class for ComparableDemo
*/
public class Menu implements Comparable <Menu>{
    public String foodName;
    public long price;
    //constructor with variables
    public Menu(String foodName, long price) {
        this.foodName = foodName;
        this.price = price;
    }
    //constructor without variables
    public Menu() {
    }
    //getter and setter
    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }
    public String toString(){
        return "foodName: " + foodName + " Price: "+price;
    }

    /**
     * sort by foodName
     */
    @Override
    public int compareTo(Menu menu) {
        //this will use for Collection.sort to sort by Foodname
        return this.getFoodName().compareTo(menu.getFoodName());
    }
}
