package Comparator;

public class Menu {
    public String foodName;
    public long price;

    //constructor with variables
    public Menu(String foodName, long price) {
        this.foodName = foodName;
        this.price = price;
    }

    //constructor without variables
    public Menu() {
    }

    //getter and setter
    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    //Make foodName and price toString
    public String toString() {
        return "foodName: " + foodName + " Price: " + price;
    }
}
