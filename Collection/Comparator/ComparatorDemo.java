package Comparator;

import Compare.Menu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Comparator;

public class ComparatorDemo   {
    public static void  main(String[] args){
        //create a new ArrayList
        List<Compare.Menu> list = new ArrayList<Compare.Menu>();
        //add elements to ArrayList
        list.add(new Compare.Menu("BeefSteak",200000));
        list.add(new Compare.Menu("Pho",50000));
        list.add(new Compare.Menu("Pasta",80000));
        list.add(new Menu("Salad",90000));

        //sort foodName ASC
        Collections.sort(list,new Comparator<Menu>(){
            @Override
            public int compare(Menu menu1, Menu menu2){ //method of Comparator
                return menu1.getFoodName().compareTo(menu2.getFoodName());
            }
        });
        //print list's element
        for(Menu menu:list){
            System.out.println(menu.toString());
        }
    }
}
