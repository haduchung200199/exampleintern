import java.util.HashMap;
import java.util.Map;


public class HashMapDemo {
    public static void main(String[] args){
        String foodName;
        //create a HashMap with key and value are String type
        Map<String, String> hashMap =  new HashMap<String, String>();
        //      key     value
        //      foodName price
        //add keys and values to HashMap
        hashMap.put("BeefSteak","200.000");
        hashMap.put("Pho", "50.000");
        //if the key is existed, the value will be changed
        hashMap.put("Pho","70.000");
        System.out.println(hashMap);
        //get the value using the key of HashMap
        System.out.println(hashMap.get("Pho"));
        //remove value and key using key to find
        hashMap.remove("BeefSteak");
        System.out.println(hashMap);
    }
}
