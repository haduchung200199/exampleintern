//Demo for ArrayList, LinkedList, List, Iterator and ListIterator
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * demo of Arraylist
 */
public class ListAndIteratorAndListerator {
    public static void main(String[] args){
        //create a ArrayList. List to ArrayList
        List<Integer> list = (List) new ArrayList<Integer>();
        //create a LinkedList
        List<Integer> list1 = new LinkedList<Integer>();
        int n;
        Scanner sc =  new Scanner(System.in);
        for(int i =0;i<5;i++){
            //input the elements for ArrayList, LinkedList, List (use the same n)
            n= sc.nextInt();
            sc.nextLine();
            //add elements to ArrayList
            list.add(n);
            //add elements to LinkedList
            list1.add(n);
        }
        //use sort the Arraylist (ASC)
        Collections.sort(list);
        System.out.println("Arraylist sort ASC: "+list);
        //sort (DESC)
        Collections.sort(list,Collections.reverseOrder());
        System.out.println("ArrayList sort DESC: "+list);
        //clear all elements from this list
        list.clear();
        //check if the ArrayList is empty
        System.out.println("ArrayList is empty: "+list.isEmpty());

        System.out.println();

        //check if the LinkedList is empty
        System.out.println("Linked list is empty: "+list1.isEmpty());
        //print the size of LinkedList
        System.out.println("Size of LinkedList: "+list1.size());
        //print if the LinkedList contains number 3
        System.out.println("List contain number 3: "+list1.contains(3));

        System.out.println();

        //Get Iterator using List's elements
        Iterator<Integer> iteratorList =  list1.iterator();
        //print Iterator element
        System.out.print("Iterarot Elements: ");
        while (iteratorList.hasNext()){ //the while will run if Iterator still has elements
            Integer element = iteratorList.next(); //go to next element
            System.out.print(element+" ");
        }

        System.out.println();
        System.out.println();

        //get Listerator using List's elements
        ListIterator<Integer>  listOfIterator =  list1.listIterator();
        //Fix the elememts ( element =  element +3).
        while(listOfIterator.hasNext()){ //Traverse the list going forward direction
            Integer element = listOfIterator.next();
            listOfIterator.set(element +3);
        }
        //print the fixed element in the reverse way
        System.out.print("ListIterator fixed elements: ");
        while(listOfIterator.hasPrevious()){ //ListIterator's pointer is at the end. So use "previous" to traverse backward
            Integer element = listOfIterator.previous();
            System.out.print(element+" ");
        }

    }

}
