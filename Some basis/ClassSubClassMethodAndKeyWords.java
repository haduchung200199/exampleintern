package com.company;

import RestaurantPackage.Restaurant;

import java.util.Scanner;
//In this demo, the class "Restaurant" will be in the different package to see how "protected" keyword actually works
/**
 * this is a demo for Class, subclass, method, and some keywords : static, protected, private, public
 * also about Interface and abstract class
 */
public class ClassSubClassMethodAndKeyWords {
    public static void main(String[] agrs){
        ClassSubClassMethodAndKeyWords classSubClassMethodAndKeyWords = new ClassSubClassMethodAndKeyWords();
        classSubClassMethodAndKeyWords.getData();
    }

    /**
     * get data from non-static class/method for main method
     */
    void getData(){
        Manager manager = new Manager();
        manager.input();
        manager.output();
    }

    /**
     * this is a subclass extends Restaurant
     */
    class Manager extends  Restaurant implements WillSayHi{
        public String name;
        public String age;
        //constructor
        public Manager(String name, String age) {
            this.name = name;
            this.age = age;
        }
        public Manager() {
        }
        //getter-setter
        public String getName() {
            return name;
        }
        public String getAge() {
            return age;
        }
        public void setName(String name) {
            this.name = name;
        }
        public void setAge(String age) {
            this.age = age;
        }

        @Override
        public void input() {
            setName("Quang");
            setAge("20");
            menu = "Food";
            status = "ok";
        }

        @Override
        public void output() {
            System.out.println(name);
            System.out.println(age);
            System.out.println(menu);
            System.out.println(status);
            super.output(); //this will call the "output" method of the parent class (Restaurant)
        }
        //Once a class implements a interface. That class MUST implements all the methods of Interface
        @Override
        public void sayHi() {
            System.out.println("Hi, this is interface demo");
        }
    }

    /**
     * interface to say Hi
     */
    public interface WillSayHi{
        void sayHi(); //interface's method doesnt have body
    }
}


