package RestaurantPackage;

import java.util.Scanner;

/**
 * abstract class that contain public, private and protected variables
 */
 public abstract class Restaurant{ //this is called super class
    public String menu; //can be access anywhere
    private String employeeInformation = "1"; //can only be accessed using method (input/output)
    protected String status; //can be access inside package and outside package(only through inheritance)
    public Restaurant(String menu, String employeeInformation, String status) {
        this.menu = menu;
        this.employeeInformation = employeeInformation;
        this.status = status;
    }

    public Restaurant() {
    }
    public String getMenu() {
        return menu;
    }
    public void setMenu(String menu) {
        this.menu = menu;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    //methods of the class
    //abstract method does not have body. if there is a abstract method (or more),
    // the class must be abstract
    public abstract void input();
    public void output(){
        System.out.println(employeeInformation); //only way to show private variable
    }
}


