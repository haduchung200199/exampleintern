package JavaIOOperation;

import java.io.*;

public class FileDemo {
    public static void main(String[] args) throws IOException {
        String fileName = "D:\\file.txt"; //name of the file
        String fileData = "File write succeed"; //data of the file
        FileDemo fileDemo = new FileDemo();
        //if file is already exist, read
        if(fileDemo.checkFileExist(fileName)){
            System.out.println("File existed");
            System.out.println("Reading File...");
            //read file for the first time
            fileDemo.fileInput(fileName);
            //write more data
            fileDemo.fileOutput(fileName,fileData);
            //read file after written new data
            fileDemo.fileInput(fileName);
        }
        //if file is not exist, create and write then read
        else {
            fileDemo.fileOutput(fileName, fileData);
            fileDemo.fileInput(fileName);
        }
    }

    /**
     * Method used for creating a file
     * @param fileName name of the file
     * @param fileData data of the file to write
     * @throws IOException
     */
    void fileOutput(String fileName, String fileData) throws IOException {
        //FileOutputStream can create a file and write data on it
        FileOutputStream fileOutputStream = new FileOutputStream(fileName,!fileName.isEmpty());//add condition to write more to the end of file
        //use to buffer an outputstream, make the performance faster
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        //convert data to byte type then write to file
        bufferedOutputStream.write(fileData.getBytes());
        bufferedOutputStream.flush();
        //close outputstream
        fileOutputStream.close();
        bufferedOutputStream.close();
    }

    /**
     * read file data and print
     * @param fileName
     */
    void fileInput(String fileName){
        try{
            FileInputStream fileInputStream = new FileInputStream(fileName);
            //use to buffer an outputstream, make the performance faster
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            int data = 0;
            //read until the end of file
            while((data = bufferedInputStream.read())!=-1){
                System.out.print((char)data);
            }
            //close the stream
            fileInputStream.close();
            bufferedInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * check if file is already exist
     * @param fileName
     * @return
     */
    boolean checkFileExist(String fileName){
        File file = new File(fileName);
        return file.exists();
    }



}
