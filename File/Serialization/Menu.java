package JavaIOOperation;

import java.io.Serial;
import java.io.Serializable;

/**
 * Class demo for Serialization
 */
public class Menu implements Serializable { //implementing Serializable make class object to be serialiazble
    public String dishName;
    public String description;
    public String image;
    //private String price;
    transient private String price; //value of this variable will not be serialized to stream
    @Serial
    private static final long serialVersionUID = 1L; //guarantee the object before and after build are the same

    //make the the variables to string to print
    public String toString(){
        return "Dishname: "+dishName + "/ Desciption: " + description +"/ Image: "+image+"/ Price: "+price;
    }
    //constructor with variables
    public Menu(String dishName, String description, String image, String price) {
        this.dishName = dishName;
        this.description = description;
        this.image = image;
        this.price = price;
    }
    //constructor without variables
    public Menu() {
    }
    //getter and setter
    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
