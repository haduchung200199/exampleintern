package JavaIOOperation;

import java.io.IOException;

public class Serialization {
    public static void main(String[] args){
        //file name
        String fileName = "D:\\Serialization.txt";
        Menu menu = new Menu();
        //set value for variables
        menu.setDishName("BeefSteak");
        menu.setDescription("Delicious");
        menu.setImage("OK");
        menu.setPrice("20000");
        //serialize menu's value to file
        try{
            SerializeAndDesrialize.serialize(menu,fileName);

        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        Menu menu1 = null;
        //deserialize from file to read to menu1
        try{
            menu1 = (Menu) SerializeAndDesrialize.desrialize(fileName);
        } catch (IOException  | ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(menu);
        System.out.println(menu1);
    }
}
