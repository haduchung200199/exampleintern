package com.company;

import java.util.Scanner;

public class Array {
    public static void main(String[] args){
        //initialize single dimension array
        int ArrayA[] = {1, 2, 3, 4, 5};
        Scanner sc = new Scanner(System.in);
        //output array value
        for(int j:ArrayA){
            System.out.print(j+" ");
        }
        //initialize multidimesional array
        int ArrayB[][] = {{1,2,3},{1,2,3,4}};
        //output multidimensional array
        //we have 2 dimension array, so there must be 2 for loops to get all the values.
        for(int i=0;i<ArrayB.length;i++){
            System.out.println("");
            for(int j=0;j<ArrayB[i].length;j++){
                System.out.print(ArrayB[i][j]+" ");
            }

        }
    }
}
