package com.company;

import java.util.Scanner;

//I havent learned ArrayList/Collection before so this example will not have Arraylist.sort or Collection.sort
public class Sort {
    public static void main(String[] args){
        int ArrayA[] = new int[5];
        Scanner scanner = new Scanner(System.in);
        //input ArrayA[]
        for (int i=0;i<A.length;i++){
            System.out.print("ArrayA["+i+"]: ");
            ArrayA[i] = sc.nextInt();
            scanner.nextLine();
        }
        //output A[] before using sort
        System.out.println("Before sort: ");
        for (int j : ArrayA) {
            System.out.print(j + " ");
        }
        //call bubble sort method
        Sort sort = new Sort();
        sort.bubbleSortDemo(ArrayA]);
        //output A[] after sort
        System.out.println("After sort: ");
        for (int i=0;i<ArrayA.length;i++){
            System.out.print(ArrayA[i]+" ");
        }
    }

    /**
     *
     * deomo for bubble sort
     */
    void bubbleSortDemo(int[] ArrayA){
        for (int i=0;i<ArrayA.length;i++){
            for(int j=0;j<ArrayA.length-i-1;j++){
                if(ArrayA[j]>ArrayA[j+1]){
                    int temp = ArrayA[j];
                    ArrayA[j] = ArrayA[j+1];
                    ArrayA[j+1] = temp;
                }
            }
        }
    }
}
