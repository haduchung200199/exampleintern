package com.company;

public class Loops {

    public static void main(String[] args) {
        int sumaryS = 0;
        Loops loops = new Loops();
        //run for demo
        sumaryS = loops.forDemo(sumaryS);// S = 100
        System.out.println("for demo: "+sumaryS);
        //run for-each demo
        loops.forEachDemo();
        //run while demo
        sumaryS = loops.whileDeomo(sumaryS); //sumaryS = 200
        System.out.println("while demo: "+sumaryS);
        //run do-while demo
        sumaryS = loops.doWhileDemo(sumaryS); //sumaryS=300
        System.out.println("do-while demo: "+sumaryS);
        //run break keyword demo
        sumaryS= loops.javaBreakKeywordDemo(sumaryS); //sumaryS=400
        System.out.println("break demo: "+sumaryS);
        //run continue keyword demo
        sumaryS = loops.javaContinueKeywordDemo(sumaryS);
        System.out.println("continue demo: "+sumaryS);
    }
    /**
    *Demo "for" loop
     */
    int forDemo(int sumaryS){
        for(int i=0;i<100;i++){
            sumaryS = sumaryS + 1;
        }
        return sumaryS;
    }

    /**
     *demo "for-each" loop
     */
    void forEachDemo(){
        int[] array = {1,2,3,4,5};
        System.out.println("array: ");
        for (int i:array) {
            System.out.print(i+" ");
        }
    }

    /**
    *demo "while" loop
     */
    int whileDeomo(int sumaryS){
        boolean flag =false;
        while (flag==false){ //must go through while condition to begin loop
            sumaryS = sumaryS +1;
            if(sumaryS==200) flag = true; //use flag to end while loop
        }
        return sumaryS;
    }

    /**
    *demo do while loop
     */
    int doWhileDemo(int sumaryS){
        do{
            sumaryS= sumaryS+1; //S+1 before checking while condition
        }while (sumaryS<300); //S = 300 -> do-while end
        return sumaryS;
    }

    /**
    *demo java "break" keyword in loop
     */
    int javaBreakKeywordDemo(int sumaryS){
        while(true){
            sumaryS = sumaryS +1;
            if(sumaryS==400) break;
        }
        return sumaryS;
    }

    /**
    **demo java "contunue" keyword in loop
     */
    int javaContinueKeywordDemo(int sumaryS){
        for(int i=0;i<5;i++){
            sumaryS = sumaryS+1;
            if(sumaryS>=400) continue; //back to for condition (i++)
            sumaryS = sumaryS +100; //this line will not be reach because S>400
        }
        return sumaryS;
    }

}
