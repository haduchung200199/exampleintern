package com.company;

import java.util.Scanner;

public class IfAndSwitchCase {
    public static void main(String[] args){
        Scanner sc= new Scanner(System.in);
        String stringS = sc.nextLine();
        IfAndSwitchCase is = new IfAndSwitchCase();
        //run if demo
        is.ifDemo(stringS);
        //run switch case demo
        boolean flag = true;
        do{
            int choice = sc.nextInt();
            sc.nextLine();
            flag = is.switchCaseDemo(choice,flag);
        }while(flag == true); //use flag to break do-while

    }

    /**
     * demo "if" condition
     */
    void ifDemo(String stringA){
        String stringB = "abc";
        if(stringB.equalsIgnoreCase(stringA)){
            System.out.println("stringA=stringB");;
        }
        else System.out.println("stringA<>stringB");
    }

    /**
     * demo switch-case
     */
    boolean switchCaseDemo(int choice,boolean flag){
        switch(choice){
            case 1:{
                System.out.println("Menu");
                break;
            }
            case 2:{
                System.out.println("Menu food");
                break;
            }
            case 3:{
                System.out.println("Menu Drink");
                break;
            }
            default: { //break when does not have any number fit 1,2 or 3
                flag = false; //condition to break do-while
                System.out.println("break");
                break;
            }
        }
        return flag;
    }
}
