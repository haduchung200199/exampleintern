package InnerClassDemo;

/**
 * Class that contains local inner class
 */
public class Restaurant {
    private String menu = "food menu";
    private String position;
    public void print(){
        String printMethod = "printer";

        /**
         * inner class inside "print" method. thí is called local inner class
         */
        class Manager{
            String workPosition = "Manager";
            //can access to Outer class variables.
            String menuName = menu;
            //access to non-final local method variables but can not change the value
            String controlPrintMethod = printMethod;

            public String getWorkPosition() {
                return workPosition;
            }
            public String getMenuName() {
                return menuName;
            }
            public String getControlPrintMethod() {
                return controlPrintMethod;
            }
        }
        //get the variables of local inner class
        Manager mn = new Manager();
        System.out.println(mn.getWorkPosition());
        System.out.println(mn.getMenuName());
        System.out.println(mn.getControlPrintMethod());
    }
}
