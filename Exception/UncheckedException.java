package ExceptionHandlingDemo;

/**
 * demo for unchecked exception
 */
public class UncheckedException {
    //unchecked exception cannot be seen at compile time, the code will throw unchecked exception at runtime
    public static void main(String[] args){
        int number1=1;
        int zero=0;
        //if we divide zero, the program will throw ArithmeticException at runtime
        //int divideResult = 1/0;
        //System.out.println(divideResult);
        String string = null;
        String string1 = "abc";
        //since string is null. the program will throw NullPointerException at runtime
//        if(string.equalsIgnoreCase(string1)){
//            System.out.println("equals");
//        }
//        else{
//            System.out.println("not equal");
//        }
        /*
        to solve this problem, we can cast string1 before string
         */
        if(string1.equalsIgnoreCase(string)){
            System.out.println("equals");
        }
        else{
            System.out.println("not equal");
        }
    }
}
