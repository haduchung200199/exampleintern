package ExceptionHandlingDemo;


import java.io.*;

/**
 * demo for checked exception
 */
public class CheckedException {
    //checked exception can be seen at runtime
    //we can throw or catch
    public static void main(String[] args) throws IOException {
        StringBuilder file = new StringBuilder();
        BufferedReader fileInput = new BufferedReader(new FileReader("D:\\file.txt")); //throws FileNotFoundException
        String data;
        while ((data = fileInput.readLine()) != null) { //throws IOException
            file.append(data);
        }
        fileInput.close();
        System.out.println(file.toString());
    }
}
