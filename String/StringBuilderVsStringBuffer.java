package String;

/**
 * demo for StringBuider and StringBuffer
 */
public class StringBuilderVsStringBuffer {
    public static void main(String[] args){
        //use to check how long each class takes time
        long startTime = System.currentTimeMillis();
        //initialize a StringBuilDer class
        StringBuilder stringBuilder = new StringBuilder("hello ");
        //use append to append string with string ("hello" + "changed")
        stringBuilder.append("changed");
        System.out.println(stringBuilder);
        //reverser the string
        stringBuilder.reverse();
        System.out.println(stringBuilder);
        //see how much time StringBuilder takes
        System.out.println(System.currentTimeMillis() - startTime + "ms");

        //initialize a StringBuffer class
        StringBuffer stringBuffer = new StringBuffer("hello ");
        //use append to append string with string ("hello" + "changed")
        stringBuffer.append("changed");
        System.out.println(stringBuffer);
        //reverse the string
        stringBuffer.reverse();
        System.out.println(stringBuffer);
        //see how much time StringBuffer takes
        System.out.println(System.currentTimeMillis() - startTime + "ms");
    }
}
