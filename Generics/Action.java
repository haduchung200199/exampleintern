package GenericsDemo;

/**
 * Generics Interface
 * @param <P>
 */
public interface Action<P> {
    public void update(P foodName);
    public void insert(P foodName);
    public void delete(P foodName);
}
