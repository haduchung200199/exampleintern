package GenericsDemo;

/**
 * class that has main method to run demo
 */
public class GenericDemo {
    public static void main(String[] agrs){
        //F and P can be any type, so we should define it into a specified parameter
        //to do this, i create a FoodMenu class extends Menu
        Menu<String,String> menu = new Menu<String,String>("BeefSteak","20000");
        System.out.println(menu);
        FoodMenu foodMenu = new FoodMenu("Pho","30000","image");
        foodMenu.delete("Pho");
        //wrong type of parameter
//        FoodMenu foodMenu1 = new FoodMenu(123,true);
    }
}
