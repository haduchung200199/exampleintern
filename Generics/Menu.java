package GenericsDemo;

/**
 * class Generic with <F and P> parameter
 * @param <F>
 * @param <P>
 */
public class Menu<F,P> {
    private F foodName;
    private P price;
    public String toString(){
        return "\nFood name: "+ foodName + "/ price: "+price;
    }
    //constructor with parameter
    public Menu(F foodName, P price) {
        this.foodName = foodName;
        this.price = price;
    }
    //constructor without parameter
    public Menu() {
    }
    //getter and setter
    public F getFoodName() {
        return foodName;
    }

    public void setFoodName(F foodName) {
        this.foodName = foodName;
    }

    public P getPrice() {
        return price;
    }

    public void setPrice(P price) {
        this.price = price;
    }
}
