package GenericsDemo;

/**
 * a class that extends Menu class Generics.
 * can define datatype for parameters. can add more generics parameters
 */
//public class FoodMenu extends Menu<String,String>{
//    public FoodMenu(String foodName, String price) { //the value of foodName and price will always be String
//        super(foodName, price);
//    }
//
//    public FoodMenu() {
//
//    }
//}

/**
 * this class can bound dataype to generics parameters
 * parameter F still be kept, but bound the other parameter to String
 * @param <F>
 */
//public class FoodMenu<F> extends Menu<F,String>{
//    //with this, foodName can be anytype, and price is always String
//    public FoodMenu(F foodName, String price) {
//        super(foodName, price);
//    }
//
//    public FoodMenu() {
//
//    }
//}

/**
 *this class can create new parameter beside parameter from Menu class
 * @param <F>
 * @param <P>
 * @param <I> the new created parameter
 */
public class FoodMenu<F,P,I> extends Menu<F,P> implements Action<P>{
    private I image;
    //constructor extends from menu class with parameter
    public FoodMenu(F foodName, P price) {
        super(foodName, price);
    }
    //constructor with new parameter I
    public FoodMenu(F foodName, P price, I image){
        super(foodName, price);
        this.image = image;
    }
    //constructor without parameter
    public FoodMenu() {

    }
    //getter and setter for image
    public I getImage() {
        return image;
    }

    public void setImage(I image) {
        this.image = image;
    }

    @Override
    public void update(P foodName) {
        System.out.println("Updating..");
    }

    @Override
    public void insert(P foodName) {
        System.out.println("Inserting...");
    }

    @Override
    public void delete(P foodName) {
        System.out.println("Deleting..");
    }
}