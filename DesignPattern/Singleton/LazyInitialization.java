package DesignPatternPackage.SingletonPattern;

/**
 * creation of instance when required. Used for singlethread
 */
public class LazyInitialization {
    //static member
    private static LazyInitialization instance;
    //private constructor - prevent to intantiate the Singleton class from outside this class
    private LazyInitialization(){
    }
    //static factory method
    public static LazyInitialization getInstance() {
        //when called, it will create a new instance.
        if(instance==null){
            instance = new LazyInitialization();
        }
        //return the created instance
        return instance;
    }
    private String information = "this is Lazy installation";
    public String getInformation() {
        return information;
    }
}
