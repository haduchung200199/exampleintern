package DesignPatternPackage.SingletonPattern;

/**
 * creation of instance at load time. Can also be called (Early Installation)
 */
public class EagerInstallation {
    //static member
    //we create instance at the beginning of loading EagerInsatallation class
    private static final EagerInstallation instance = new EagerInstallation();
    //private constructor - prevent to intantiate the Singleton class from outside this class
    private EagerInstallation(){
    }
    //static factory method
    public static EagerInstallation getInstance() {
        //as we have created instance before, now just have to return it
        return instance;
    }
    private String information = "this is Eager installation";
    public String getInformation() {
        return information;
    }
}
