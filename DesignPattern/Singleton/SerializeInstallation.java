package DesignPatternPackage.SingletonPattern;

import java.io.Serializable;

/**
 * Class for serialization singleton
 */
public class SerializeInstallation implements Serializable {
    //serial version for serialization
    private static final long serialVersionUID = -15L;
    //private constructor
    private SerializeInstallation(){
    }
    //create static member
    private static class SingletonInstanceCreator{
        private static final SerializeInstallation instance =  new SerializeInstallation();
    }
    //static factory method to getInstance
    public static SerializeInstallation getInstance(){
        return SingletonInstanceCreator.instance;
    }
}
