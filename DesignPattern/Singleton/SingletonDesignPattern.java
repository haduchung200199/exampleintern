package DesignPatternPackage.SingletonPattern;

import java.io.*;

/**
 * demo for Singleton design pattern
 * to create a singleton design pattern, we need static member of class, private constructor and static factory method
 */
public class SingletonDesignPattern {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //get information from EagerInstallation Singleton
        EagerInstallation eagerInstallation = EagerInstallation.getInstance() ;
        System.out.println(eagerInstallation.getInformation());
        //get information from Lazy Installation Singleton
        LazyInitialization lazyInitialization = LazyInitialization.getInstance();
        System.out.println(lazyInitialization.getInformation());

        //instantiate to write
        SerializeInstallation instanceToWrite = SerializeInstallation.getInstance();
        ObjectOutput objectOutput = new ObjectOutputStream(new FileOutputStream("D://singleton.txt"));
        objectOutput.writeObject(instanceToWrite);
        objectOutput.close();
        //instantiate to read
        ObjectInput objectInput = new ObjectInputStream(new FileInputStream("D://singleton.txt"));
        SerializeInstallation instanceToRead = (SerializeInstallation) objectInput.readObject();
        objectInput.close();
        //show 2 different hascode to prove they are different
        System.out.println(instanceToWrite.hashCode());
        System.out.println(instanceToRead.hashCode());

    }
}
