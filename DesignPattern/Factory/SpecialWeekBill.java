package DesignPatternPackage.FactoryPattern;

/**
 * Bill used for special week
 */
public class SpecialWeekBill extends Bill{
    @Override
    public long getPayMent(long price) {
        return (long)(price*0.7);
    }
}
