package DesignPatternPackage.FactoryPattern;

/**
 * demo for factory method pattern
 */
public class FactoryDesignPattern {
    public static void main(String[] args){
        BillFactory billFactory = new BillFactory();
        Bill bill = billFactory.getBill("VIP");
        System.out.println("VIP Bill: "+bill.getPayMent(+10000));
        bill = billFactory.getBill("SpecialWeek");
        System.out.println("Specialweek bill: "+bill.getPayMent(10000));
        bill = billFactory.getBill("");
        System.out.println("Normal bill:"+bill.getPayMent(10000) );
    }
}
