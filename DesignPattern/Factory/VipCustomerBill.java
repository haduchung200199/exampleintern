package DesignPatternPackage.FactoryPattern;

/**
 * Bill used for VIP customer
 */
public class VipCustomerBill extends Bill{
    @Override
    public long getPayMent(long price) {
        return (long) (price*0.8);
    }
}
