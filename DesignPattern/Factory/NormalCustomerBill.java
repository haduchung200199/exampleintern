package DesignPatternPackage.FactoryPattern;

/**
 * Bill for normal customers
 */
public class NormalCustomerBill extends Bill{
    @Override
    public long getPayMent(long price) {
        return price;
    }
}
