package DesignPatternPackage.FactoryPattern;

/**
 * normal Bill
 */
public abstract class Bill {
    public abstract long getPayMent(long price);
}
