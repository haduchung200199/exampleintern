package DesignPatternPackage.FactoryPattern;

/**
 * control to create object
 */
public class BillFactory {
    public Bill getBill(String billName){
        if(billName.equalsIgnoreCase("VIP"))
            return new VipCustomerBill();
        else if(billName.equalsIgnoreCase("SpecialWeek"))
            return new SpecialWeekBill();
        else return new NormalCustomerBill();
    }
}
