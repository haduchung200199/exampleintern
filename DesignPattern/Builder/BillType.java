package DesignPatternPackage.BuilderPattern;

import java.util.ArrayList;
import java.util.List;

/**
 * list contain type of bill
 */
public class BillType {
    private List<Payment> list = new ArrayList<Payment>();
    public void addList(Payment payment){
        list.add(payment);
    }
    public void getPrice(){
        for(Payment payment: list){
            payment.makePayment();
        }
    }
    public void showBill(){
       for (Payment payment: list){
           System.out.println("Bill Type: "+payment.getName());
           System.out.println("Price: "+payment.makePayment());
       }

    }
}
