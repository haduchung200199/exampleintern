package DesignPatternPackage.BuilderPattern;

/**
 * demo for Builder design pattern
 */
public class BuilderDesignPattern {
    public static void main(String[] args){
        BillBuilder billBuilder = new BillBuilder();
        //call vip bill
        BillType vipBill = billBuilder.getVipBill();
        vipBill.showBill();
        //call normal bill
        BillType normalBill = billBuilder.getNormalBill();
        normalBill.showBill();
    }
}
