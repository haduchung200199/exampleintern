package DesignPatternPackage.BuilderPattern;

/**
 * interface to make payment
 */
public interface Payment {
    public String getName();
    public long makePayment();
}
