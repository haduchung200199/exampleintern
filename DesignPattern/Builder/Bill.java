package DesignPatternPackage.BuilderPattern;

/**
 * Bill contains different payment medthod
 */
public abstract class Bill implements Payment{
    public abstract String getName();
    public abstract long makePayment();
}
