package DesignPatternPackage.BuilderPattern;

/**
 * Bill for normal customer
 */
public class NormalBill extends Bill{
    @Override
    public String getName(){
        return "Normal customer bill";
    }
    @Override
    public long makePayment() {
        return 10000;
    }
}
