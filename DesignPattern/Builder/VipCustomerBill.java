package DesignPatternPackage.BuilderPattern;

/**
 * Bill for VIP customer
 */
public class VipCustomerBill extends Bill{
    @Override
    public String getName(){
        return "VIP customer bill";
    }


    @Override
    public long makePayment() {
        return (long)(10000*0.8);
    }
}
