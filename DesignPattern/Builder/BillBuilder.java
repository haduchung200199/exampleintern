package DesignPatternPackage.BuilderPattern;

/**
 * to call type of bill
 */
public class BillBuilder {
    public BillType getNormalBill(){
        BillType billType = new BillType();
        billType.addList(new NormalBill());
        return billType;
    }
    public BillType getVipBill(){
        BillType billType = new BillType();
        billType.addList(new VipCustomerBill());
        return billType;
    }

}
