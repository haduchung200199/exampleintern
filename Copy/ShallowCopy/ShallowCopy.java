package Compare;

public class ShallowCopy {
    public static void main(String[] args) throws CloneNotSupportedException{
        ShallowCopy shallowCopy = new ShallowCopy();
        shallowCopy.implementCloningObject();
    }
    /**
     * Make ShallowCopy for main class
     */
    void implementCloningObject() throws CloneNotSupportedException {
        MakeShallowCopy makeShallowCopy = new MakeShallowCopy();
        makeShallowCopy.number3 = 10;
        makeShallowCopy.number4 = 20;
        makeShallowCopy.variableToCopy.number1 = 30;
        makeShallowCopy.variableToCopy.number2 = 40;
        MakeShallowCopy makeShallowCopy1 = (MakeShallowCopy) makeShallowCopy.clone(); //clone makeShallowCopy to makeShallowCopy1
        makeShallowCopy1.number3 = 100; //this is expected that makeShallowCopy1 will not reflect change to makeShallowCopy (change primivite type)
        makeShallowCopy1.variableToCopy.number1 = 300; //makeShallowCopy1 will reflect change to makeShallowCopy (change object type)
        System.out.println("makeShallowCopy: " + makeShallowCopy.number3 + " "+ makeShallowCopy.number4 +
                " " + makeShallowCopy.variableToCopy.number1 + " " + makeShallowCopy.variableToCopy.number2);
        //makeShallowCopy1 is the copy of makeShallowCopy. so the unchanged variable will be the same with makeShallowCopy
        System.out.println("makeShallowCopy1: " + makeShallowCopy1.number3 + " "+ makeShallowCopy1.number4 +
                " " + makeShallowCopy1.variableToCopy.number1 + " " + makeShallowCopy1.variableToCopy.number2);
    }
}
