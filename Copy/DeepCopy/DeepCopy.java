package Comparator;

public class DeepCopy {
    public static void main(String[] args) throws CloneNotSupportedException {
        DeepCopy deepCopy = new DeepCopy();
        deepCopy.implementDeepCopy();
    }

    /**
     * use to implement deepcopy for main method
     */
    void implementDeepCopy() throws CloneNotSupportedException{
        MakeDeepCopy makeDeepCopy  = new MakeDeepCopy();
        makeDeepCopy.number3 = 10;
        makeDeepCopy.number4 = 20;
        makeDeepCopy.variableToCopy.number1 = 30;
        makeDeepCopy.variableToCopy.number2 = 40;

        // a clone of makeDeepCopy
        MakeDeepCopy makeDeepCopy1 = (MakeDeepCopy)makeDeepCopy.Clone();
        //change in primitive type of t2 will not reflect t1
        makeDeepCopy1.number3 = 100;
        //even change in object type will still not reflect t1. this is the difference between deepcopy and shallow copy
        makeDeepCopy1.variableToCopy.number1 = 400;
        //expect only t2 field has valuechanges, t1 still be the same as we've initialized before
        System.out.println("makeDeepCopy: "+makeDeepCopy.number3+" "+makeDeepCopy.number4+" "+makeDeepCopy.variableToCopy.number1+" "+makeDeepCopy.variableToCopy.number2);
        System.out.println("makeDeepCopy1: "+makeDeepCopy1.number3+" "+makeDeepCopy1.number4+" "+makeDeepCopy1.variableToCopy.number1+" "+makeDeepCopy1.variableToCopy.number2);
    }
}
