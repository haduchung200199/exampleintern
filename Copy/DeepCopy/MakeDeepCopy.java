package Comparator;

/**
 * make deep copy
 */
public class MakeDeepCopy implements Cloneable{
    public int number3;
    public int number4;
    VariableToCopy variableToCopy = new VariableToCopy();
    public Object Clone() throws CloneNotSupportedException {
        MakeDeepCopy makeDeepCopy  =  (MakeDeepCopy) super.clone();
        makeDeepCopy.variableToCopy = new VariableToCopy();
        //create object for c and assign it to shallow copy obbtained
        makeDeepCopy.variableToCopy.number1 = variableToCopy.number1;
        makeDeepCopy.variableToCopy.number2 = variableToCopy.number2;
        return makeDeepCopy;
    }
}
